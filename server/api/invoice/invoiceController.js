import { get, isEmpty } from "lodash";

import { errorHandler, successHandler } from "../../helpers/responseHandlers";
import invoiceModel from "./invoiceModel";

export const list = async (req, res) => {
  try {
    const query = get(req, "query.query", {});
    const projection = get(req, "query.projection", {});
    const options = get(req, "query.options", {});

    const result = await invoiceModel.find(query, projection, options);

    return successHandler(res, result);
  } catch (err) {
    return errorHandler(res, err);
  }
};

export const details = async (req, res) => {
  try {
    const _id = get(req, "params._id", '');

    const result = await invoiceModel.findOne({ _id });

    return successHandler(res, result);
  } catch (err) {
    return errorHandler(res, err);
  }
};

export const add = async (req, res) => {
  try {
    const { patientName, patientAge, patientPhoneNumber = "", patientGender, tests } = req.body;

    if (!patientName || !patientAge || !patientGender || !tests || isEmpty(tests)) {
      return errorHandler(res, { message: "Please provide valid info." });
    }

    const result = await invoiceModel.create({ patientName, patientAge, patientPhoneNumber, patientGender, tests, });
    const finalResult = await invoiceModel.findOne({ _id: result._id }, {}, {});
    return successHandler(res, finalResult);
  } catch (err) {
    return errorHandler(res, err);
  }
};
