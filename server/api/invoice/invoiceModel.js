import mongoose from "mongoose";
import { requiredFieldError } from '../../helpers/errorUtils';

const Schema = mongoose.Schema;

const invoiceSchema = new Schema({
  patientName: {
    type: String,
    required: [true, requiredFieldError("Patient-Name")]
  },
  patientAge: {
    type: Number,
    required: [true, requiredFieldError("Patient age")]
  },
  patientPhoneNumber: {
    type: Number,
  },
  patientGender: {
    type: String,
    enum: ["M", "F"],
    required: [true, requiredFieldError("Patient Gender")]
  },
  tests: [{
    type: Schema.Types.ObjectId,
    ref: "test",
  }],
  createdOn: { type: Date, default: Date.now },
});

let autoPopulate = function (next) {
  this.populate({
    path: "tests",
    select: "testName testAmount"
  });
  next();
};

invoiceSchema.pre("find", autoPopulate).pre("findOne", autoPopulate);


export default mongoose.model("invoice", invoiceSchema, "invoice");
