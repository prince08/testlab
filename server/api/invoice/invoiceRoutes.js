import express from "express";
import { list, add, details } from './invoiceController';

const router = express.Router();

router.get('/', list);

router.get('/details/:_id', details);

router.get('/list', list);

router.post('/add', add);

export default router;
