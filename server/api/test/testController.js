import { get, isEmpty } from "lodash";

import { errorHandler, successHandler } from "../../helpers/responseHandlers";
import testModel from "./testModel";

export const list = async (req, res) => {
  try {
    const query = get(req, "query.query", {});
    const projection = get(req, "query.projection", {});
    const options = get(req, "query.options", {});

    const result = await testModel.find(query, projection, options);

    return successHandler(res, result);
  } catch (err) {
    return errorHandler(res, err);
  }
};

export const add = async (req, res) => {
  try {
    const { testName, testAmount } = req.body;

    if (!testName) {
      return errorHandler(res, { message: "Please provide valid Test-Name." });
    }
    if (isNaN(testAmount)) {
      return errorHandler(res, { message: "Please provide valid Test-Amount." });
    }
    if (await ( checkTestExists(testName) )) {
      return errorHandler(res, { message: "Test is already added in DB." });
    }

    const result = await testModel.create({ testName, testAmount });

    return successHandler(res, result);
  } catch (err) {
    return errorHandler(res, err);
  }
};

export const remove = async (req, res) => {
  try {
    const { testId } = req.body;

    if (!testId) {
      return errorHandler(res, { message: "Please provide valid Test info." });
    }

    const result = await testModel.remove({ _id: testId });

    return successHandler(res, result);
  } catch (err) {
    return errorHandler(res, err);
  }
};

// validations
const checkTestExists = async (testName) => {
  try {
    const testList = await testModel.findOne({ testName });

    return testList && !isEmpty(testList);
  } catch (err) {
    return false;
  }
};