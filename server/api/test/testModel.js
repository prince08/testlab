import mongoose from "mongoose";
import { requiredFieldError } from '../../helpers/errorUtils';

const Schema = mongoose.Schema;

const testSchema = new Schema({
  testName: {
    type: String,
    required: [true, requiredFieldError("Name of the Test")]
  },
  testAmount: {
    type: Number,
    required: [true, requiredFieldError("Test amount")]
  }
});

export default mongoose.model("test", testSchema, "test");
