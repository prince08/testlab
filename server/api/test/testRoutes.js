import express from "express";
import { list, add, remove } from './testController';

const router = express.Router();

router.get('/', list);
router.get('/list', list);

router.post('/add', add);

router.post('/remove', remove);

export default router;
