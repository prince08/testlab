import express from "express";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import cors from "cors";

import config from "./config/config";
import test from "./api/test/testRoutes";
import invoice from "./api/invoice/invoiceRoutes";
import Logger from './helpers/logger'

const { mongoUrl = process.env.MONGO_URL, clientHost } = config;

/** ---------- Mongoose operations -------------- */
mongoose.connect(mongoUrl, { useMongoClient: true });
mongoose.Promise = Promise;

mongoose.connection.on("connected", () => {
  Logger.info("MONGO connection is successfully done.");
});

mongoose.connection.on("error", error => {
  Logger.info("Mongoose connection error --> ", JSON.stringify(error));
  setTimeout(() => {
    /* On mongo connection error, exit from running server*/
    process.exit(0);
  }, 1000);
});

mongoose.connection.on("disconnected", () => {
  Logger.info("Mongoose connection disconnected --> ", new Date());
  /* need to check how tp reconnect with mongo*/
});
/** ---------- Mongoose operations end -------------- */

const app = express();

/** implementing middleware */
//cross origin setting
app.use(cors({ origin: clientHost }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

/** error handler --> Handles error for multer-filterFile/mongoose connection etc.*/
app.use(function (err, req, res, next) {
  Logger.info("In error handler --> ", new Date(), JSON.stringify(err));
  let conn = mongoose.connection;

  if (conn.states[conn._readyState] === "disconnected") {
    err.message = "There is a problem in connecting to database, please contact to administrator.";
  }
  // render the error page
  err.status = err.status ? err.status : 500;
  res.status(err.status).json(err);
});

/** routes */
app.use("/test", test);
app.use("/invoice", invoice);

/**Middleware to catch 404 */
app.use(function (req, res, next) {
  const err = { status: 404, message: "Route Not found" };
  res.status(err.status).json(err);
});

process.on("uncaughtException", err => {
  Logger.info("uncaught exception in Parent Process", err);
});

process.on("SIGINT", () => {
  Logger.info("Received SIGINT.  Press Control-D to exit.");
  mongoose.connection.close(function () {
    process.exit(0);
  });
});

export default app;
