export default {
  serverHost: "http://localhost:4000",
  clientHost: "http://localhost:3000",
  mongoUrl: "mongodb://localhost:27017/labTests",
}