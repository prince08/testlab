import {isArray} from 'lodash';

export function requiredFieldError(fieldName) {
  return `${fieldName} is required.`;
}

export function minLengthFieldError(fieldName) {
  return `You should select one of the given ${fieldName}.`;
}

export function uniqueFieldError(fieldName) {
  return `${fieldName} value should be unique.`;
}

export function selectOneOfGivenMessage(fieldName) {
  return `Please select one of given ${fieldName}.`;
}

export function checkingDependencyMessage(dependentFieldName, fieldName) {
  return `Error on checking ${dependentFieldName} for selected ${fieldName}.`;
}

export function successfullDeleteMessage(fieldName) {
  return `Successfully deleted selected ${fieldName}.`;
}

export function alreadyUsedMessage(fieldNames = []) {
  if (isArray(fieldNames) && fieldNames.length > 0) {
    return `${fieldNames.toString()} ${fieldNames.length > 1 ? 'are' : 'is'} already used.`;
  } else {
    return `Selected ${fieldNames} is already used.`;
  }
}