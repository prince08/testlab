const logData = (...data) => {
  console.log(...data);
};

export const Logger = {
  info: logData.bind(null, 'INFO :- '),
  warn: logData.bind(null, 'WARN :- '),
  error: logData.bind(null, 'ERROR :- '),
};

export default Logger;