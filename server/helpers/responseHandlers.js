export const errorHandler = function (res, error, status = 500) {
  return res.status(status).json(error);
};

export const successHandler = function (res, data, status = 200) {
  return res.status(status).json(data);
};
