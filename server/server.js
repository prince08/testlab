#!/usr/bin/env node
/**
 * Module dependencies.
 */
import app from "./app";
import Logger from './helpers/logger'

let port = process.env.PORT || "4000";
port = normalizePort(port);

app.listen(port, onListening);
app.on('error', onError);

function onListening() {
  Logger.info("Listening on " + port);
}

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    return null;
  }
  if (port >= 0) {
    return port;
  }
}

function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  switch (error.code) {
    case "EACCES":
      Logger.info(port + " requires elevated privileges");
      process.exit(0);
      break;
    case "EADDRINUSE":
      Logger.info(port + " is already in use");
      process.exit(0);
      break;
    default:
      throw error;
      process.exit(0);
  }
}

