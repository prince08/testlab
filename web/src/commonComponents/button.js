import React from "react";
import MaterialButton from '@material-ui/core/Button';

export const Button = (props) => {
  const { text, onPress, color, variant, className } = props;

  return (
    <MaterialButton
      className={`btn ${className}`}
      variant={variant || "outlined"}
      color={color || "primary"}
      onClick={onPress}
    >
      {text}
    </MaterialButton>
  )
};

export default Button;