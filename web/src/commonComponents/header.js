import React from "react";
import { AppBar } from "material-ui";
import { withRouter } from 'react-router-dom';

import { APP_TITLE } from "../utilities/constants";

const Header = (props) => {
  const { history } = props;

  return (
    <AppBar
      className={"dashboard-header"}
      title={APP_TITLE}
      onTitleClick={() => history.replace('/dashboard')}
      iconElementLeft={<div/>}
    />
  )
};

export default withRouter(Header);
