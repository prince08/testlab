import React from 'react';
import TextField from '@material-ui/core/TextField';

class Input extends React.Component {
  state = {
    value: '',
  };

  handleChange = ({ target: { value } }) => {
    this.setState({ value });
  };

  render() {
    const { label, id,variant, className, ...otherProps } = this.props;
    const { value } = this.state;

    return (
      <TextField
        id={id || "standard-name"}
        label={label || ""}
        value={value}
        variant={variant || "outlined"}
        className={`f ${className}`}
        onChange={this.handleChange}
        {...otherProps}
      />
    );
  }
}

export default Input;
