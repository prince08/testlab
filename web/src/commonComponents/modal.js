import React from 'react';
import MaterialModal from '@material-ui/core/Modal';

export const Modal = (props) => {
  const { open, children, ...otherProps } = props;

  return (
    <MaterialModal
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      open={open}
      {...otherProps}
    >
      {children || null}
    </MaterialModal>
  );
};

export default Modal;
