import React from 'react';
import isEmpty from 'lodash/isEmpty';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import MaterialInput from '@material-ui/core/Input';
import Chip from '@material-ui/core/Chip';

import { Button, Input } from "../../../commonComponents";

export default class AddInvoice extends React.PureComponent {

  state = {
    patientName: '',
    patientAge: '',
    patientPhoneNumber: "",
    patientGender: 'M',
    selectedTests: [],
    genders: [{ value: 'M', label: 'Male' }, { value: 'F', label: 'Female' }],
  };

  handleChange = (name, type, range = 10000000) => ({ target: { value } }) => {
    if (type === 'number') {
      value = parseFloat(value);
      value = !isNaN(value) ? value : 0;
      if (value > range || value < 0) return alert(`Please enter valid ${name}`);
    }
    this.setState({ [name]: value });
  };

  updateTests = ({ target: { value } }) => {
    this.setState({ selectedTests: value });
  };

  handleSubmit = () => {
    const { addInvoice } = this.props;
    const { patientName, patientAge, patientGender, selectedTests } = this.state;

    if (patientName && patientAge >= 0 && patientGender && !isEmpty(selectedTests)) {
      return addInvoice(this.state, () => {
        this.setState({
          patientName: '',
          patientAge: '',
          patientPhoneNumber: "",
          patientGender: 'M',
          selectedTests: [],
        })
      })
    }
  };

  render() {
    const { patientName, patientAge, genders, patientPhoneNumber, patientGender, selectedTests } = this.state;
    const { testsList } = this.props;

    return (
      <div className='f1 m20 aCenter jCenter'>
        <div className='fdc w50 aCenter jCenter bgWhite p20'>
          <Input
            label='Patient name'
            className='m10'
            value={patientName}
            onChange={this.handleChange('patientName')}
          />
          <Input
            label='Patient age'
            className='m10'
            value={patientAge}
            onChange={this.handleChange('patientAge', 'number', 100)}
          />
          <Input
            label='Phone number'
            className='m10'
            value={patientPhoneNumber}
            onChange={this.handleChange('patientPhoneNumber', 'number', 999999999999)}
          />
          <div className='f1 fdc m20'>
            <RadioGroup
              className={'f fdr'}
              value={patientGender}
              onChange={this.handleChange('patientGender', 'enum')}
            >
              {genders.map(({ label, value }) => (
                <FormControlLabel
                  key={value}
                  value={value}
                  control={<Radio/>}
                  label={label}
                />
              ))}
            </RadioGroup>
            <div className='f fr aCenter'>
              <div className='f1'>Select tests :-</div>
              <div className='f2'>
                <SelectTestsList
                  testsList={testsList}
                  value={selectedTests}
                  onChange={this.updateTests}
                />
              </div>
            </div>
          </div>
          <div className='f1 jEnd'>
            <Button
              text={'Submit'}
              className='m10 w20'
              onPress={this.handleSubmit}
            />
          </div>
        </div>
      </div>
    )
  }
}

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const SelectTestsList = (props) => {
  const { testsList, value = [], onChange } = props;

  return (
    <Select
      multiple
      value={value}
      onChange={onChange}
      className='f1'
      input={<MaterialInput id="select-multiple-chip"/>}
      renderValue={selected => (
        <div className='f frw'>
          {testsList.map(({ _id, testName, testAmount }) => (
            selected.indexOf(_id) > -1
              ? (
                <Chip
                  key={_id}
                  className='m5'
                  label={`${testName} :- ${testAmount}`}
                />
              )
              : null
          ))}
        </div>
      )}
      MenuProps={MenuProps}
    >
      {
        testsList.map(({ testName, testAmount, _id }) => (
          <MenuItem key={_id} value={_id}>
            {`${testName} :- ${testAmount}`}
          </MenuItem>
        ))
      }
    </Select>
  )
};
