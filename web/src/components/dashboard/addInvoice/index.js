import { connect } from 'react-redux';
import AddTest from "./addInvoice";
import { addInvoice } from "../../../redux/actions";

const mapStateToProps = (state) => ( {
  testsList: state.testsList,
} );
const mapDispatchToProps = {
  addInvoice
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTest);
