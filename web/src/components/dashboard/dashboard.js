import React from 'react';

import { Button, Header, Modal } from '../../commonComponents';
import AddTest from "./addInvoice";
import InvoicesList from "./invoicesList";

export default class Dashboard extends React.PureComponent {
  state = {
    openModal: false
  };

  componentDidMount() {
    const { getTestList, } = this.props;
    getTestList();
  }

  toggleModal = () => {
    this.setState(({ openModal }) => ( { openModal: !openModal } ));
  };

  goToTests = () => {
    const { history } = this.props;
    history.replace('/labTests');
  };

  render() {
    const { openModal } = this.state;

    return (
      <div className='f1 fdc'>
        <Header/>
        <div className='fdr m20'>
          <Button  text={'Tests'} onPress={this.goToTests} />
          <Button className='ml20' text={'Add new bill'} onPress={this.toggleModal}/>
        </div>
        <InvoicesList/>

        <Modal open={openModal} onClose={this.toggleModal}>
          <AddTest/>
        </Modal>

      </div>
    )
  }
}
