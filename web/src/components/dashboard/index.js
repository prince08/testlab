import { connect } from 'react-redux';
import Dashboard from "./dashboard";
import { getTestList } from "../../redux/actions";

const mapStateToProps = () => ( {} );
const mapDispatchToProps = {
  getTestList,
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
