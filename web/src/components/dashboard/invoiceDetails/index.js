import { connect } from 'react-redux';
import InvoicesDetail from "./invoiceDetails";
import { getInvoiceDetails } from "../../../redux/actions";

const mapStateToProps = ({ invoiceDetails }) => ( { invoiceDetails, } );
const mapDispatchToProps = {
  getInvoiceDetails
};

export default connect(mapStateToProps, mapDispatchToProps)(InvoicesDetail);
