import React from 'react';
import moment from 'moment';
import './style.css';
import logo from './logo.png'
import { Header } from "../../../commonComponents";
import { COMPANY_ADDRESS, COMPANY_CONTACT_NUMBER, COMPANY_EMAIL, COMPANY_NAME } from "../../../utilities/constants";

export default class InvoiceDetails extends React.PureComponent {
  componentDidMount() {
    const { getInvoiceDetails, match: { params: { _id } } } = this.props;

    getInvoiceDetails(_id);
  }

  render() {
    if (!this.props.invoiceDetails) {
      return <div/>
    }

    const { patientName, patientAge, patientPhoneNumber, createdOn, tests } = this.props.invoiceDetails;

    const currency = 'Rs.';
    const footerMessage = 'Invoice was created on a computer and is invalid without the signature and seal.';
    const taxPercentage = 0.10;
    const subTotal = tests.reduce((acc, { testAmount }) => acc + testAmount, 0);
    const taxAmount = ( 0.10 * subTotal );
    const total = ( subTotal + taxAmount );
    const createdOnDateString = moment(createdOn).format('LLL');

    return (
      <div className='f1 fdc'>
        <Header/>
        <div className='invoiceContainer'>
          <header className="clearfix">
            <div id="logo">
              <img src={logo} alt="Logo"/>
            </div>
            <div id="company">
              <h2 className="name">{COMPANY_NAME}</h2>
              <div>{COMPANY_ADDRESS}</div>
              <div>{COMPANY_CONTACT_NUMBER}</div>
              <div><a href="mailto:company@example.com">{COMPANY_EMAIL}</a></div>
            </div>
          </header>
          <main className='testsListContainer'>
            <div id="details" className="clearfix">
              <div id="client">
                <div className="to">INVOICE TO:</div>
                <h2 className="name">{patientName}</h2>
                <div className="address">Age :- {patientAge}</div>
                <div className="address">{patientPhoneNumber}</div>
                {/*<div className="email"><a href="mailto:john@example.com">john@example.com</a></div>*/}
              </div>
              <div id="invoice">
                <h1>INVOICE 3-2-1</h1>
                <div className="date">Date of Invoice: {createdOnDateString}</div>
              </div>
            </div>
            <table border="0" cellSpacing="0" cellPadding="0">
              <thead>
              <tr>
                <th className="no">#</th>
                <th className="desc">TEST NAME</th>
                <th className="unit">UNIT PRICE</th>
                <th className="qty">QUANTITY</th>
                <th className="total">TOTAL</th>
              </tr>
              </thead>
              <tbody>
              {
                tests.map((test, index) => {
                  const { _id, testName, testAmount, } = test;

                  return (
                    <tr key={_id}>
                      <td className="no">{index}</td>
                      <td className="desc">
                        <h3>{testName}</h3>
                        {/*Creating a recognizable design solution based on the*/}
                        {/*company's existing visual identity*/}
                      </td>
                      <td className="unit">{currency} {testAmount.toFixed(2)}</td>
                      <td className="qty">01</td>
                      <td className="total">{currency} {testAmount.toFixed(2)}</td>
                    </tr>
                  )
                })
              }
              </tbody>
              <tfoot>
              <tr>
                <td colSpan="2"/>
                <td colSpan="2">SUBTOTAL</td>
                <td>{currency} {subTotal.toFixed(2)}</td>
              </tr>
              <tr>
                <td colSpan="2"/>
                <td colSpan="2">TAX {taxPercentage}%</td>
                <td>{currency} {taxAmount.toFixed(2)}</td>
              </tr>
              <tr>
                <td colSpan="2"/>
                <td colSpan="2">GRAND TOTAL</td>
                <td>{currency} {total.toFixed(2)}</td>
              </tr>
              </tfoot>
            </table>
            <div className="signature">Signature</div>
          </main>
          <footer className='invoiceFooter'>{footerMessage}</footer>
        </div>
      </div>
    )
  }
}
