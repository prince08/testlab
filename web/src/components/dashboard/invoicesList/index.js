import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import InvoicesList from "./invoicesList";
import { getInvoiceList } from "../../../redux/actions";

const mapStateToProps = ({ invoicesList }) => ( { invoicesList, } );
const mapDispatchToProps = {
  getInvoiceList
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(InvoicesList));
