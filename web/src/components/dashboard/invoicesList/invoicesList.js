import React from 'react';
import objectGet from 'lodash/get';
import moment from 'moment';
import './style.css';

export default class InvoicesList extends React.PureComponent {

  componentDidMount() {
    const { getInvoiceList } = this.props;

    getInvoiceList();
  }

  onRowClick = (_id) => {
    const { history } = this.props;
    history.push(`/invoice/${_id}`);
  };

  render() {
    const { invoicesList = [] } = this.props;

    return (
      <div className='f1 fdc ml20 mr20'>
        <table className='f1'>
          <tbody className='f1 fdc'>
          <tr className='f1'>
            <TableHead text={'Bill date'}/>
            <TableHead text={'Patient name'}/>
            <TableHead text={'Age'} className='f0_5'/>
            <TableHead text={'Gender'} className='f0_5'/>
            <TableHead text={'Phone Number'} className='f0_5'/>
            <TableHead text={'Tests taken'}/>
          </tr>
          {
            invoicesList.map((data, index) => (
              <ListRow
                key={objectGet(data, '_id', index)}
                data={data}
                onRowClick={this.onRowClick.bind(this, objectGet(data, '_id', index))}
              />
            ))
          }
          </tbody>
        </table>
      </div>
    )
  }
}

const ListRow = ({ data, onRowClick }) => {
  const { createdOn, patientName, patientAge, patientPhoneNumber, patientGender, tests } = data;

  return (
    <tr className='f1' onClick={onRowClick}>
      <TableData text={moment(createdOn).format('LLL')}/>
      <TableData text={patientName}/>
      <TableData text={patientAge} className='f0_5'/>
      <TableData text={patientGender} className={'f0_5'}/>
      <TableData text={patientPhoneNumber} className='f0_5'/>
      <td className='f1 fdc'>{
        tests.map(({ testName, _id }) => ( <div key={_id}>{`${testName},`}</div> ))
      }</td>
    </tr>
  )
};

const TableHead = ({ text, className = '' }) => <th className={`f1 ${className}`}>{text}</th>;
const TableData = ({ text, className = '' }) => <td className={`f1 ${className}`}>{text}</td>;
