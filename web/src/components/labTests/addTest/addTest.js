import React from 'react';
import { Input, Button } from "../../../commonComponents";

export default class AddTest extends React.PureComponent {

  state = {
    testName: '',
    testAmount: 0,
  };

  handleChange = (name, type) => ({ target: { value } }) => {
    if (type === 'number') {
      value = parseFloat(value);
      value = !isNaN(value) ? value : 0;
      if (value > 10000000 || value < 0) return alert('Please enter valid amount');
    }
    this.setState({ [name]: value });
  };

  handleSubmit = () => {
    const { addTest } = this.props;
    const { testName, testAmount } = this.state;
    if (testName && testAmount >= 0) {
      return addTest({ testName, testAmount }, () => {
        this.setState({ testName: "", testAmount: 0 })
      })
    }
  };

  render() {
    const { testAmount, testName } = this.state;
    return (
      <div className='f1 m20 aCenter jCenter'>
        <div className='fdc w50 aCenter jCenter bgWhite p20'>
          <Input
            label='Test name'
            className='m10'
            value={testName}
            onChange={this.handleChange('testName')}
          />
          <Input
            label='Test amount'
            className='m10'
            value={testAmount}
            onChange={this.handleChange('testAmount', 'number')}
          />
          <div className='f1 jEnd'>
            <Button
              text={'Submit'}
              className='m10 w20'
              onPress={this.handleSubmit}
            />
          </div>
        </div>
      </div>
    )
  }
}