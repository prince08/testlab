import { connect } from 'react-redux';
import AddTest from "./addTest";
import { addTest } from "../../../redux/actions";

const mapStateToProps = () => ( {} );
const mapDispatchToPorps = {
  addTest
};

export default connect(mapStateToProps, mapDispatchToPorps)(AddTest);
