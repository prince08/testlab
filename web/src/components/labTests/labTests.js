import React from 'react';

import { Button, Modal, Header } from '../../commonComponents';

import TestsList from './testsList';
import AddTest from './addTest';

export default class LabTests extends React.PureComponent {
  state = {
    openModal: false
  };

  toggleModal = () => {
    this.setState(({ openModal }) => ( { openModal: !openModal } ));
  };

  render() {
    const { openModal } = this.state;

    return (
      <div className='f1 fdc'>
        <Header/>
        <div className='f1 fdc m20'>
          <Button text={'Add new test'} onPress={this.toggleModal}/>
          <TestsList/>
        </div>
        <Modal open={openModal} onClose={this.toggleModal}>
          <AddTest/>
        </Modal>
      </div>
    )
  }
}
