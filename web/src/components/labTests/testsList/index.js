import { connect } from 'react-redux';
import TestsList from "./testsList";
import { getTestList, deleteTest } from "../../../redux/actions";

const mapStateToProps = ({ testsList }) => ( {
  testsList,
} );
const mapDispatchToProps = {
  getTestList,
  deleteTest,
};

export default connect(mapStateToProps, mapDispatchToProps)(TestsList);
