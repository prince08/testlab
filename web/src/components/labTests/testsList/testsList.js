import React from 'react';
import objectGet from 'lodash/get';
import './style.css';
import { Button } from "../../../commonComponents";

export default class TestsList extends React.PureComponent {

  componentDidMount() {
    const { getTestList } = this.props;

    getTestList();
  }

  removeTest = (testId, index) => () => {
    const { deleteTest } = this.props;
    const response = window.confirm('Are you sure to delete this test.');
    if (response) {
      deleteTest({ testId, index });
    }
  };

  render() {
    const { testsList } = this.props;

    return (
      <div className='f1 fdc ml20'>
        <table>
          <tbody>
          <tr>
            <th>Test name</th>
            <th>Test amount</th>
            <th>Action</th>
          </tr>
          {
            testsList.map((data, index) => (
              <ListRow
                key={objectGet(data, '_id', index)}
                data={data}
                onPress={this.removeTest(objectGet(data, '_id', index), index)}
              />
            ))
          }
          </tbody>
        </table>
      </div>
    )
  }
}

const ListRow = ({ data, onPress }) => {
  const { testName, testAmount } = data;

  return (
    <tr>
      <td>{testName}</td>
      <td>{testAmount}</td>
      <td>
        <Button
          text={'delete'}
          onPress={onPress}
        />
      </td>
    </tr>
  )
};