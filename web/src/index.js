import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { getMuiTheme, lightBaseTheme, MuiThemeProvider } from 'material-ui/styles';

import './style/index.css';
import Dashboard from "./components/dashboard";
import InvoiceDetails from "./components/dashboard/invoiceDetails";
import LabTests from "./components/labTests/labTests";
import { NoMatch } from "./commonComponents";
import store from './redux/store';

const Main = () => (
  <Provider store={store}>
    <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Dashboard}/>
          <Route exact path="/dashboard" component={Dashboard}/>
          <Route exact path="/labTests" component={LabTests}/>
          <Route exact path="/invoice/:_id" component={InvoiceDetails}/>
          <Route component={NoMatch}/>
        </Switch>
      </BrowserRouter>
    </MuiThemeProvider>
  </Provider>
);

ReactDOM.render(<Main/>, document.getElementById('root'));
