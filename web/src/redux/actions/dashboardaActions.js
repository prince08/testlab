import objectGet from 'lodash/get';

import { get, post } from '../../utilities/api';
import { ADD_INVOICE_GET_URI, INVOICE_LIST_GET_URI } from "../../utilities/urlConstants";
import { isOkResponse } from "../../utilities/validations";
import { ADD_INVOICE_LIST, } from "../reduxConstants";

export const getInvoiceList = () => {
  return async (dispatch) => {
    try {
      const { data, status } = await get(INVOICE_LIST_GET_URI);
      if (isOkResponse(status)) {
        return dispatch({ type: ADD_INVOICE_LIST, data });
      }
      return dispatch({ type: ADD_INVOICE_LIST, data: [] });
    } catch (err) {
      console.log("-inside error getInvoiceList :- ", err);
      return dispatch({ type: ADD_INVOICE_LIST, data: [] });
    }
  };
};

export const addInvoice = (statePayload, cb) => {
  return async (dispatch, getState) => {
    try {
      const { patientName, patientAge, patientPhoneNumber = "", patientGender, selectedTests } = statePayload;
      const payload = { patientName, patientAge, patientPhoneNumber, patientGender, tests: selectedTests };
      const { data, status } = await post(ADD_INVOICE_GET_URI, payload);

      if (isOkResponse(status)) {
        cb();
        return dispatch({
          type: ADD_INVOICE_LIST,
          data: [...getState().invoicesList, data],
        });
      }
    } catch (err) {
      console.log("-inside error addInvoice :- ", err);
      const message = objectGet(err, 'response.data.message', '');
      if (message) return alert(message);
    }
  };
};
