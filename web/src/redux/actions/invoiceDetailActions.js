import { get } from "../../utilities/api";
import { INVOICE_DETAILS_GET_URI } from "../../utilities/urlConstants";
import { isOkResponse } from "../../utilities/validations";
import { ADD_INVOICE_DETAILS } from "../reduxConstants";

export const getInvoiceDetails = (_id) => {
  return async (dispatch) => {
    try {
      const { data, status } = await get(`${INVOICE_DETAILS_GET_URI}/${_id}`);
      if (isOkResponse(status)) {
        return dispatch({ type: ADD_INVOICE_DETAILS, data });
      }
      return dispatch({ type: ADD_INVOICE_DETAILS, data: {} });
    } catch (err) {
      console.log("-inside error getInvoiceDetails :- ", err);
      return dispatch({ type: ADD_INVOICE_DETAILS, data: {} });
    }
  };
};
