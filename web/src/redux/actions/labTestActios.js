import objectGet from 'lodash/get';

import { get, post } from '../../utilities/api';
import { TEST_LIST_GET_URI, ADD_TEST_POST_URI, REMOVE_TEST_POST_URI } from "../../utilities/urlConstants";
import { isOkResponse } from "../../utilities/validations";
import { ADD_TEST_LIST, } from "../reduxConstants";

export const getTestList = () => {
  return async (dispatch) => {
    try {
      const { data, status } = await get(TEST_LIST_GET_URI);
      if (isOkResponse(status)) {
        return dispatch({ type: ADD_TEST_LIST, data });
      }
      return dispatch({ type: ADD_TEST_LIST, data: [] });
    } catch (err) {
      console.log("-inside error getTestList :- ", err);
      return dispatch({ type: ADD_TEST_LIST, data: [] });
    }
  };
};

export const addTest = ({ testName, testAmount }, cb) => {
  return async (dispatch, getState) => {
    try {
      const payload = { testName, testAmount };
      const { data, status } = await post(ADD_TEST_POST_URI, payload);

      if (isOkResponse(status)) {
        cb();
        return dispatch({
          type: ADD_TEST_LIST,
          data: [...getState().testsList, data],
        });
      }
    } catch (err) {
      console.log("-inside error addTest :- ", err);
      const message = objectGet(err, 'response.data.message', '');
      if (message) return alert(message);
    }
  };
};

export const deleteTest = ({ testId, index }) => {
  return async (dispatch, getState) => {
    try {
      const payload = { testId };
      const { status } = await post(REMOVE_TEST_POST_URI, payload);
      let testsList = getState().testsList;

      if (isOkResponse(status)) {
        testsList.splice(index, 1);
        return dispatch({
          type: ADD_TEST_LIST,
          data: [...testsList],
        })
      }
    } catch (err) {
      console.log("-inside error addTest :- ", err);
      const message = objectGet(err, 'response.data.message', '');
      if (message) return alert(message);
    }
  };
};