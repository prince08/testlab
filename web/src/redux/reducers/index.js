import { ADD_INVOICE_LIST, ADD_TEST_LIST,ADD_INVOICE_DETAILS } from "../reduxConstants";

export default function (state = {}, action) {
  const { type, data } = action;

  switch (type) {
    case ADD_TEST_LIST:
      return { ...state, testsList: data };
    case ADD_INVOICE_LIST:
      return { ...state, invoicesList: data };
    case ADD_INVOICE_DETAILS:
      return { ...state, invoiceDetails: data };
    default:
      return state;
  }
}

