import { applyMiddleware, compose, createStore } from "redux";
import reducer from "./reducers/index";
import thunk from "redux-thunk";

const INITIAL_STATE = {
  testsList: [],
  invoicesList: [],
  InvoiceDetails: {},
};
const enhancer = compose(applyMiddleware(thunk));

const store = createStore(reducer, INITIAL_STATE, enhancer);

export default store;
