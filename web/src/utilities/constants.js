export const PAGE_NOT_FOUND = '404 Page not found.';
export const APP_TITLE = 'DR. PATH LABS';
export const COMPANY_NAME = 'DR. PATH LABS';
export const COMPANY_ADDRESS = 'Hissar';
export const COMPANY_CONTACT_NUMBER = '+91-12398756';
export const COMPANY_EMAIL = 'company@example.com';
