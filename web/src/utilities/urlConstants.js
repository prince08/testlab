export const TEST_LIST_GET_URI = '/test/';
export const ADD_TEST_POST_URI = '/test/add';
export const REMOVE_TEST_POST_URI = '/test/remove';
export const INVOICE_LIST_GET_URI = '/invoice/list';
export const ADD_INVOICE_GET_URI = '/invoice/add';
export const INVOICE_DETAILS_GET_URI = '/invoice/details';
